package com.coolplace.checkthis.checkthiscoolplace

import android.os.Looper
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import com.coolplace.checkthis.checkthiscoolplace.map.GoogleLocationApiManager
import com.nhaarman.mockitokotlin2.spy
import com.nhaarman.mockitokotlin2.whenever
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    @Rule
    @JvmField
    val mainActivity = ActivityTestRule<MainActivity>(MainActivity::class.java)

    @Test
    fun useAppContext() {
        // Context of the app under test.
        //val appContext = InstrumentationRegistry.getContext()

        val googleLocationApiManager = GoogleLocationApiManager(mainActivity.activity, Looper.getMainLooper())
        spy(googleLocationApiManager)

        whenever(googleLocationApiManager.checkPermission())
                .thenReturn(false)

        assertEquals(true, googleLocationApiManager.checkPermission())
    }
}
