package com.coolplace.checkthis.checkthiscoolplace

import com.coolplace.checkthis.checkthiscoolplace.login.LoginPresenter
import com.coolplace.checkthis.checkthiscoolplace.login.LoginView
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}
