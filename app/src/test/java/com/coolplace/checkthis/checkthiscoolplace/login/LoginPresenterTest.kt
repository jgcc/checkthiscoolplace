package com.coolplace.checkthis.checkthiscoolplace.login

import com.coolplace.checkthis.checkthiscoolplace.BaseRepository
import com.nhaarman.mockitokotlin2.*
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class LoginPresenterTest {

    private val viewMock = mock<LoginView>()
    private val repositoryMock = mock<BaseRepository>()
    private lateinit var mPresenter: LoginPresenter

    @Before
    fun setUp() {
        mPresenter = LoginPresenter(viewMock, repositoryMock)
    }

    @Test
    fun checkIfValidEmail() {
        val email = arrayOf("myemail221@aolmail.com",
                "email@gmail.com",
                "e.mai_l@hotmail.com",
                "test#@yahoo.com",
                "strongburrito@cock.li")
        assert(mPresenter.isValidMail(email[1]))
        assert(mPresenter.isValidMail(email[2]))
        assert(mPresenter.isValidMail(email[3]))
        assert(mPresenter.isValidMail(email[4]))
        assert(mPresenter.isValidMail(email[0]))
    }

    @Test
    fun checkFailWhenEmailIsInvalid() {
        val email = arrayOf("myEmail221aolmail.com",
                "email@gmail",
                "e.mai_l.com",
                "test_@_yahoo.com",
                "strongburrito")
        assertEquals(false, mPresenter.isValidMail(email[0]))
        assertEquals(false, mPresenter.isValidMail(email[1]))
        assertEquals(false, mPresenter.isValidMail(email[2]))
        assertEquals(false, mPresenter.isValidMail(email[3]))
        assertEquals(false, mPresenter.isValidMail(email[4]))
    }

    @Test
    fun whenEverythingIsFINE() {
        val email = "email@gmail.com"
        val password = "somePasswordStrongPassword"
        whenever(repositoryMock.login(email, password)).thenReturn(true)
        mPresenter.login(email, password)

        verify(viewMock, atLeastOnce()).onLoginSuccess()
    }

    @Test
    fun whenRegisterIsOK() {
        val email = "email@gmail.com"
        val password = "somePasswordStrongPassword"
        whenever(repositoryMock.register(email, password)).thenReturn(true)
        mPresenter.register(email, password)

        verify(viewMock, atLeastOnce()).onLoginSuccess()
    }

    @Test
    fun checkWhenEmailIsEmpty() {
        val emptyEmail = ""
        val somePassword = "somePassword"
        mPresenter.login(emptyEmail, somePassword)

        argumentCaptor<ErrorType>().apply {
            verify(viewMock).showInputError(capture())
            assertEquals(ErrorType.EMPTY_EMAIL, firstValue)
        }
    }

    @Test
    fun checkWhenEmailIsInvalid() {
        val invalidEmail = "thisIsnotEmail"
        val somePassword = "somePassword"
        mPresenter.login(invalidEmail, somePassword)

        argumentCaptor<ErrorType>().apply {
            verify(viewMock).showInputError(capture())
            assertEquals(ErrorType.INVALID_EMAIL, firstValue)
        }
    }

    @Test
    fun checkWhenEmailIsNotRegistered() {
        val emailNotRegistered = "somemail@email.com"
        val somePassword = "somePassword"

        mPresenter.login(emailNotRegistered, somePassword)

        argumentCaptor<ErrorType>().apply {
            verify(viewMock).showInputError(capture())
            assertEquals(ErrorType.NOT_REGISTERED, firstValue)
        }
    }

    @Test
    fun checkWhenPasswordIsEmpty() {
        val email = "somemail@email.com"
        val emptyPassword = ""
        mPresenter.login(email, emptyPassword)

        argumentCaptor<ErrorType>().apply {
            verify(viewMock).showInputError(capture())
            assertEquals(ErrorType.EMPTY_PASSWORD, firstValue)
        }
    }

    @Test
    fun checkWhenPasswordIsToShort() {
        val email = "somemail@email.com"
        val emptyPassword = "ASS"
        mPresenter.login(email, emptyPassword)

        argumentCaptor<ErrorType>().apply {
            verify(viewMock).showInputError(capture())
            assertEquals(ErrorType.INVALID_PASSWORD, firstValue)
        }
    }

    @After
    fun tearDown() {

    }
}