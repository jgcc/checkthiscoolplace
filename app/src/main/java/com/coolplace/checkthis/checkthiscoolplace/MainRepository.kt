package com.coolplace.checkthis.checkthiscoolplace

import com.coolplace.checkthis.checkthiscoolplace.models.CoolPlace

class MainRepository : BaseRepository {

    override fun login(userEmail: String, userPassword: String): Boolean {
        return true
    }

    override fun register(userEmail: String, userPassword: String): Boolean {
        return true
    }

    override fun getAll(): Array<CoolPlace> {
        return arrayOf()
    }

    override fun findBy(entity: CoolPlace): Boolean {
        return true;
    }

}