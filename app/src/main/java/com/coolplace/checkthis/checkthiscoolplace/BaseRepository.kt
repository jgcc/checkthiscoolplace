package com.coolplace.checkthis.checkthiscoolplace

import com.coolplace.checkthis.checkthiscoolplace.models.CoolPlace

interface BaseRepository {
    fun login(userEmail: String, userPassword: String): Boolean
    fun register(userEmail: String, userPassword: String): Boolean
    fun getAll(): Array<CoolPlace>
    fun findBy(entity: CoolPlace): Boolean
}