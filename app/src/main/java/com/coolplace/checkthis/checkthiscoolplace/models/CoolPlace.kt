package com.coolplace.checkthis.checkthiscoolplace.models

import com.google.android.gms.location.Geofence

data class CoolPlace(val userID: String = "",
                     val title: String = "",
                     val description: String = "",
                     val coolCount: Int = 0,
                     val geoFence: Geofence? = null,
                     val latitude: Double = 0.0,
                     val longitude: Double = 0.0,
                     val uuid: String = "")
