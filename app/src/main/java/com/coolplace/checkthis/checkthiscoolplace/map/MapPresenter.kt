package com.coolplace.checkthis.checkthiscoolplace.map

import android.location.Location

class MapPresenter(private var view: MapView, private val googleLocationApiManager: GoogleLocationApiManager) {

    init {
        view.setPresenter(this)
    }

    fun startLocationUpdates() {
        googleLocationApiManager.startLocationUpdates()
    }

    fun checkDistance(locationA: Location, locationB: Location): Boolean {
        val distance = floatArrayOf(0.0f)
        Location.distanceBetween(locationB.latitude, locationB.longitude, locationA.latitude, locationA.longitude, distance)
        return distance[0] < 30 // 30 meters
    }

    fun getLastLocation() {
        googleLocationApiManager.getLastLocation()
    }

}