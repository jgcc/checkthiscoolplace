package com.coolplace.checkthis.checkthiscoolplace.login

import com.coolplace.checkthis.checkthiscoolplace.BaseView

interface LoginView : BaseView <LoginPresenter> {
    fun showInputError(errorType: ErrorType): ErrorType
    fun showLoadingProgress(show: Boolean)
    fun onLoginSuccess()
    fun onLoginFail()
}