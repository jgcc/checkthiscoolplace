package com.coolplace.checkthis.checkthiscoolplace

import android.os.Bundle
import android.os.Looper
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.coolplace.checkthis.checkthiscoolplace.login.LoginFragment
import com.coolplace.checkthis.checkthiscoolplace.login.LoginPresenter
import com.coolplace.checkthis.checkthiscoolplace.login.LoginView
import com.coolplace.checkthis.checkthiscoolplace.map.GoogleLocationApiManager
import com.coolplace.checkthis.checkthiscoolplace.map.MapFragment
import com.coolplace.checkthis.checkthiscoolplace.map.MapPresenter
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity(), FirebaseAuth.AuthStateListener {

    private val MAP_TAG = "MY_CONTENT_TAG"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
    }

    override fun onAuthStateChanged(firebaseAuth: FirebaseAuth) {
        val mainRepository = MainRepository()
        var fragment:Fragment = LoginFragment()

        firebaseAuth.currentUser?.let {
            val mapFragment = MapFragment()
            val googleLocationApiManager = GoogleLocationApiManager(this,
                    Looper.getMainLooper(),
                    mapFragment)
            val mapPresenter = MapPresenter(mapFragment, googleLocationApiManager)
            fragment = mapFragment
        } ?: kotlin.run {
            val loginPresenter = LoginPresenter(fragment as LoginView, mainRepository)
        }

        supportFragmentManager.beginTransaction()
                .replace(R.id.main_content, fragment, MAP_TAG)
                .commit()
    }

    override fun onStart() {
        super.onStart()
        FirebaseAuth.getInstance().addAuthStateListener(this)
    }

    override fun onStop() {
        super.onStop()
        FirebaseAuth.getInstance().removeAuthStateListener(this)
    }
}
