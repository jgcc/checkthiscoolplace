package com.coolplace.checkthis.checkthiscoolplace.map

import android.app.DialogFragment
import android.content.Context
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.coolplace.checkthis.checkthiscoolplace.R
import com.coolplace.checkthis.checkthiscoolplace.ui.BaseDialogHelper

class AddCoolPlaceDialogHelper(context: Context): BaseDialogHelper() {

    override val dialogView by lazy {
        LayoutInflater.from(context)
                .inflate(R.layout.fragment_add_cool_place, null)
    }

    override val builder: AlertDialog.Builder  = AlertDialog.Builder(context)
            .setView(dialogView)

    val textViewAddress by lazy {
        dialogView.findViewById<TextView>(R.id.textViewAddress)
    }

    val  editTextTitle by lazy {
        dialogView.findViewById<EditText>(R.id.editTextTitle)
    }

    val  editTextDescription by lazy {
        dialogView.findViewById<EditText>(R.id.editTextDescription)
    }

    val  buttonSave by lazy {
        dialogView.findViewById<Button>(R.id.buttonSave)
    }

    fun doneIconClickListener(callback: (() -> Unit)? = null) =
            with(buttonSave) {
                setOnClickListener { callback }
            }
}
