package com.coolplace.checkthis.checkthiscoolplace.map

import android.content.Context
import android.os.Looper
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationServices.getFusedLocationProviderClient

val LOCATION_REQUEST_INTERVAL = 10L * 1000L
val LOCATION_REQUEST_FASTEST_INTERVAL = 2L * 1000L
val LOCATION_REQUEST_PRIORITY = LocationRequest.PRIORITY_HIGH_ACCURACY
val GOOGLE_LOCATION_REQUEST_CODE = 666

class GoogleLocationApiManager(private val context: Context,
                               private val looper: Looper,
                               private val mapView: MapView) {

    private val locationRequest = LocationRequest().apply {
        interval = LOCATION_REQUEST_INTERVAL
        fastestInterval = LOCATION_REQUEST_FASTEST_INTERVAL
        priority = LOCATION_REQUEST_PRIORITY
    }

    private val locationSettingsRequest = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
            .build()

    private val settingsClient = LocationServices.getSettingsClient(context)

    private val locationCallback = object: LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult?.let {
                mapView.onLocationChange(it.lastLocation)
            }
        }
    }

    fun startLocationUpdates() {
        settingsClient.checkLocationSettings(locationSettingsRequest)
        if (mapView.checkPermission()) {
            FusedLocationProviderClient(context)
                    .requestLocationUpdates(locationRequest, locationCallback, looper)

        }
    }

    fun getLastLocation() {
        val locationClient = getFusedLocationProviderClient(context)
        if (mapView.checkPermission()) {
            locationClient.lastLocation
                    .addOnSuccessListener {
                        if (it != null)
                            mapView.onLocationChange(it)
                    }
                    .addOnFailureListener {
                        mapView.showError()
                        it.printStackTrace();
                    }
        }
    }

}