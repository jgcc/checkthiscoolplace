package com.coolplace.checkthis.checkthiscoolplace.login

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.coolplace.checkthis.checkthiscoolplace.R
import kotlinx.android.synthetic.main.fragment_login.*

class LoginFragment : Fragment(), View.OnClickListener, LoginView {

    private lateinit var mPresenter: LoginPresenter;

    override fun setPresenter(presenter: LoginPresenter) {
        mPresenter = presenter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonLogin.setOnClickListener(this)
        buttonRegister.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }

    override fun onClick(v: View?) {
        val userEmail = editTextEmail.text.toString()
        val userPassword = editTextPassword.text.toString()
        when(v?.id) {
            R.id.buttonLogin -> mPresenter.login(userEmail, userPassword)
            R.id.buttonRegister -> mPresenter.register(userEmail, userPassword)
        }
    }

    override fun showInputError(errorType: ErrorType): ErrorType {
        if (errorType == ErrorType.EMPTY_EMAIL ||
                errorType == ErrorType.INVALID_EMAIL ||
                errorType == ErrorType.NOT_REGISTERED) {
            editTextEmail.error = getString(errorType.resource)
        } else if (errorType == ErrorType.EMPTY_PASSWORD ||
                errorType == ErrorType.INVALID_PASSWORD) {
            editTextPassword.error = getString(errorType.resource)
        }
        return  errorType
    }

    override fun showLoadingProgress(show: Boolean) {
        editTextEmail.isEnabled = !show
        editTextPassword.isEnabled = !show
        buttonRegister.isEnabled = !show
        buttonLogin.isEnabled = !show
        progressLoading.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onLoginSuccess() {
        // noting for now
    }

    override fun onLoginFail() {
        Toast.makeText(context, "Failed to connect to server", Toast.LENGTH_LONG).show()
    }
}