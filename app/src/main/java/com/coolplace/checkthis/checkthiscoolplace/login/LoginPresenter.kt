package com.coolplace.checkthis.checkthiscoolplace.login

import android.util.Log
import com.coolplace.checkthis.checkthiscoolplace.BaseRepository
import com.coolplace.checkthis.checkthiscoolplace.R
import com.google.firebase.auth.FirebaseAuth

enum class ErrorType(val resource: Int) {
    NONE(0),
    EMPTY_EMAIL(R.string.msg_error_email_empty),
    EMPTY_PASSWORD(R.string.msg_error_password_empty),
    INVALID_EMAIL(R.string.msg_error_email_invalid),
    INVALID_PASSWORD(R.string.msg_error_password_to_short),
    NOT_REGISTERED(R.string.msg_error_email_not_registered)
}

class LoginPresenter(private var view: LoginView?, private val repository: BaseRepository) {

    val MAX_PASSWORD_LENGTH = 4

    var isSessionStarted:Boolean = false
        private set

    init {
        view?.setPresenter(this)
    }

    fun login(userEmail: String, userPassword: String) {
        view?.showLoadingProgress(true)

        val errorType = checkError(userEmail, userPassword)
        if (errorType != ErrorType.NONE) {
            view?.showLoadingProgress(false)
            view?.showInputError(errorType)
            return
        }

        FirebaseAuth.getInstance()
                .signInWithEmailAndPassword(userEmail, userPassword)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        view?.onLoginSuccess()
                    } else {
                        view?.showLoadingProgress(false)
                        Log.d("PRSENTER", it.result.toString())
                        view?.onLoginFail()
                    }
                }
    }

    fun register(userEmail: String, userPassword: String) {
        view?.showLoadingProgress(true)

        val errorType = checkError(userEmail, userPassword)
        if (errorType != ErrorType.NONE) {
            view?.showLoadingProgress(false)
            view?.showInputError(errorType)
            return
        }
        if (repository.register(userEmail, userPassword)) {
            view?.onLoginSuccess()
        } else {
            view?.showLoadingProgress(false)
            view?.onLoginFail()
        }
    }

    fun isValidMail(email: String): Boolean {
        val emailRegex = Regex("(?:[a-z0-9!#\$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#\$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
        return emailRegex.matches(email)
    }

    fun checkError(userEmail: String, userPassword: String): ErrorType {
        if (userEmail.isEmpty()) return ErrorType.EMPTY_EMAIL

        if (!isValidMail(userEmail))  return ErrorType.INVALID_EMAIL

        if (userPassword.isEmpty()) return ErrorType.EMPTY_PASSWORD

        if (userPassword.length < MAX_PASSWORD_LENGTH) return ErrorType.INVALID_PASSWORD

        return ErrorType.NONE
    }

}