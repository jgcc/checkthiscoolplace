package com.coolplace.checkthis.checkthiscoolplace

interface BaseView<T> {
    fun setPresenter(presenter: T)
}