package com.coolplace.checkthis.checkthiscoolplace.map

import android.content.Context
import android.location.Location
import com.coolplace.checkthis.checkthiscoolplace.BaseView

interface MapView: BaseView<MapPresenter> {
    fun getMapViewContext(): Context
    fun onLocationChange(location: Location)
    fun showError()
    fun checkPermission(): Boolean
    fun showAddLocationDialog()
}