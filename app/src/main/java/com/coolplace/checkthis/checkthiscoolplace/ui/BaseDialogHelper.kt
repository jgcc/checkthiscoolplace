package com.coolplace.checkthis.checkthiscoolplace.ui

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AlertDialog
import android.view.View

abstract class BaseDialogHelper {

    abstract val dialogView: View
    abstract val builder: AlertDialog.Builder

    open var cancelable: Boolean = true
    open var isBackgroundTransparent = false

    open var dialog: AlertDialog? = null

    open fun create(): AlertDialog {
        dialog = builder
                .setCancelable(cancelable)
                .create()

        if (isBackgroundTransparent)
            dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        return dialog!!
    }

    open fun onCancelListener(callback: () -> Unit): AlertDialog.Builder? = builder.setOnCancelListener {
        callback
    }
}