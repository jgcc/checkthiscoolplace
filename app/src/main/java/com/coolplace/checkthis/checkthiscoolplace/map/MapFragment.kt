package com.coolplace.checkthis.checkthiscoolplace.map

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.*
import com.coolplace.checkthis.checkthiscoolplace.R
import com.coolplace.checkthis.checkthiscoolplace.models.CoolPlace
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_map.*
import java.util.*

class MapFragment : Fragment(), MapView,  OnMapReadyCallback, ValueEventListener {

    private lateinit var presenter: MapPresenter
    private lateinit var mMap: GoogleMap

    private var addCoolPlaceDialog: AlertDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

        FirebaseDatabase.getInstance()
                .reference
                .child("CoolPlaces")
                .addValueEventListener(this)

        btnLocation.setOnClickListener {
            presenter.getLastLocation()
        }

        btnAddLocation.setOnClickListener {
            showAddLocationDialog()
        }

        FirebaseDatabase.getInstance().reference
                .child("COOLPLACES")
                .addListenerForSingleValueEvent(this)

        /*
        val coolPlace = CoolPlace("12",
                "Zocalo",
                "bla", 1, null, 19.4327, -99.1334, UUID.randomUUID().toString())

        val key = FirebaseDatabase.getInstance().reference
                .child("COOLPLACES").push().key

        FirebaseDatabase.getInstance().reference
                .child("COOLPLACES")
                .child(key!!)
                .setValue(coolPlace)*/
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if(requestCode == GOOGLE_LOCATION_REQUEST_CODE) {
            if(grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                // gps-related task you need to do.
            }
        }
    }

    override fun onDataChange(dataSnapshot: DataSnapshot) {
        dataSnapshot.children.forEach { snapshot ->
            val coolPlace = snapshot.getValue<CoolPlace>(CoolPlace::class.java)
            coolPlace?.let {
                mMap.addMarker(MarkerOptions()
                        .position(LatLng(it.latitude, it.longitude))
                        .title(it.title))
                println(it)
            }
        }
    }

    override fun onCancelled(databaseError: DatabaseError) {
        Log.e("DatabaseError", databaseError.message)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.map_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId) {
            R.id.action_close_session -> {
                FirebaseAuth.getInstance().signOut()
                true
            } else -> {
                super.onOptionsItemSelected(item)
            }
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.uiSettings.apply {
            isCompassEnabled = false
            isMyLocationButtonEnabled = false
            isZoomControlsEnabled = false
            isMapToolbarEnabled = false
            isMapToolbarEnabled = false
        }
        googleMap.setInfoWindowAdapter(CoolPlaceInfoWindowAdapter(requireContext()))
        mMap = googleMap

        if (checkPermission()) {
            mMap.isMyLocationEnabled = true
        }
        presenter.startLocationUpdates()
        presenter.getLastLocation()
    }

    override fun setPresenter(presenter: MapPresenter) {
        this.presenter = presenter
    }

    override fun getMapViewContext(): Context {
         return requireContext()
    }

    override fun onLocationChange(location: Location) {
        val latLng = LatLng(location.latitude, location.longitude)
        mMap.addMarker(MarkerOptions().position(latLng).title("My position"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15.0f))
    }

    override fun checkPermission(): Boolean {
        val permission = ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION)
        return if (permission == PackageManager.PERMISSION_GRANTED) {
            true
        } else {
            // Request Location permission
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), GOOGLE_LOCATION_REQUEST_CODE)
            false
        }
    }

    override fun showError() {

    }

    override fun showAddLocationDialog() {
        if (addCoolPlaceDialog == null) {
            addCoolPlaceDialog = showAddCoolPlace {
                cancelable = true
                doneIconClickListener {
                    Log.d("click", "asasas")
                }
            }
        }
        addCoolPlaceDialog?.show()
    }

    inline fun Fragment.showAddCoolPlace(callback: AddCoolPlaceDialogHelper.() -> Unit): AlertDialog =
        AddCoolPlaceDialogHelper(requireContext()).apply {
            callback()
        }.create()
}