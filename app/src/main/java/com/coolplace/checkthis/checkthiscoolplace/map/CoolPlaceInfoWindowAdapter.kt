package com.coolplace.checkthis.checkthiscoolplace.map

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.coolplace.checkthis.checkthiscoolplace.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import java.util.*

class CoolPlaceInfoWindowAdapter(val context: Context): GoogleMap.InfoWindowAdapter {

    override fun getInfoContents(marker: Marker?): View {
        val coolPlaceView = LayoutInflater.from(context).inflate(R.layout.view_cool_place_details, null)

        val txtTitle = coolPlaceView.findViewById<TextView>(R.id.title)
        val txtDescription = coolPlaceView.findViewById<TextView>(R.id.description)
        val txtAddress = coolPlaceView.findViewById<TextView>(R.id.address)
        val txtCity = coolPlaceView.findViewById<TextView>(R.id.city)

        val geocoder = Geocoder(context, Locale.getDefault())

        marker?.let {
            val addresses = geocoder.getFromLocation(it.position.latitude,
                    it.position.longitude,
                    1)

            if (!addresses.isEmpty()) {
                val address = addresses[0]

                txtTitle.text = address.featureName
                txtDescription.text = address.locality
                txtAddress.text = address.subLocality
                txtCity.text = address.countryName

            }
        }
        return coolPlaceView
    }

    override fun getInfoWindow(marker: Marker?): View? {
        return  null //LayoutInflater.from(context).inflate(R.layout.view_cool_place_details, null)
    }
}